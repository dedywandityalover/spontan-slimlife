<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Slimlife | Login</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='' rel='stylesheet'>
        <style>



body {
background: white;
}

a{
    text-decoration: none;
    color: rgb(142, 184, 53)
}

@media only screen and (max-width: 767px) {
.hide-on-mobile {
display: none;
}
}

.login-box {
background:url({{ asset('slimlife/background.png') }});;
background-size: cover;
background-position: center;
padding: 50px;
margin: 50px auto;
min-height: 700px;
-webkit-box-shadow: 0 2px 60px -5px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 60px -5px rgba(0, 0, 0, 0.1);
}

.logo {
font-size: 54px;
text-align: center;
margin-bottom: 50px;
}

.logo .logo-font {
color: rgb(142, 184, 53);
}

@media only screen and (max-width: 767px) {
.logo {
font-size: 34px;
}
}

.header-title {
text-align: center;
margin-bottom: 50px;
}

.login-form {
max-width: 300px;
margin: 0 auto;
}

.login-form .form-control {
border-radius: 0;
margin-bottom: 30px;
}

.login-form .form-group {
position: relative;
}

.login-form .form-group .forgot-password {
position: absolute;
top: 6px;
right: 15px;
}

.login-form .btn {
border-radius: 0;
-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
margin-bottom: 30px;
}

.login-form .btn.btn-primary {
background: rgb(142, 184, 53);
border-color: rgb(142, 184, 53);
}

.slider-feature-card {
background: #fff;
max-width: 280px;
margin: 0 auto;
padding: 30px;
text-align: center;
-webkit-box-shadow: 0 2px 25px -3px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 25px -3px rgba(0, 0, 0, 0.1);
}

.slider-feature-card img {
height: 80px;
margin-top: 30px;
margin-bottom: 30px;
}

.slider-feature-card h3,
.slider-feature-card p {
margin-bottom: 30px;
}

.carousel-indicators {
bottom: -50px;
}

.carousel-indicators li {
cursor: pointer;
}</style>
        <script type='text/javascript' src=''></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'></script>
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js'></script>
    </head>
    <body oncontextmenu='return false' class='snippet-body'>
    <section class="body">
<div class="container">
<div class="login-box">
<div class="row">
<div class="col-sm-6">
<div class="logo">
<life class="logo-font">Registrasi
</div>
</div>
<div class="row">
<div class="col-sm-6">
<br>
<form action="/registrasi" method="POST" class="signin-form login-form">
    @csrf

<div class="form-group mb-3">
<input type="text" class="form-control @error('name')is-invalid @enderror" name="name" placeholder="Masukan Username" required value="{{ old('name') }}">
@error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
</div>


<div class="form-group mb-3">
<input type="text" class="form-control @error('email')is-invalid @enderror" name="email" placeholder="Masukan Email" required value="{{ old('email') }}">
@error('email')<div class="invalid-feedback">{{ $message }}</div>@enderror
</div>
<div class="form-group mb-3">
<input type="password" class="form-control @error('password')is-invalid @enderror" name="password" placeholder="Masukan Password" required value="{{ old('password') }}">
@error('password')<div class="invalid-feedback">{{ $message }}</div>@enderror
</div>
<div class="form-group">
  <button type="submit" class="form-control btn btn-primary rounded submit px-3">Registrasi</button>
</div>

<div class="form-group">
    <div class="text-center">Sudah Memiliki Akun? <a href="/login">Login</a></div>
</div>
</form>
</div>
<div class="col-sm-6 hide-on-mobile">
<div id="demo" class="carousel slide" data-ride="carousel">
<!-- Indicators -->
<ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
</ul>
<!-- The slideshow -->
<div class="carousel-inner">
    <div class="carousel-item active">
        <div class="slider-feature-card" style="background: rgb(142, 184, 53)">
            <img src="{{ asset('slimlife/Slimlife-background.png') }}" alt="">
            <h3 class="slider-title">Slimlife</h3>
            <p class="slider-description">Cara Sehat Dalam Satu Genggaman</p>
        </div>
    </div>
    <div class="carousel-item">
        <div class="slider-feature-card" style="background: rgb(142, 184, 53)">
            <img src="{{ asset('slimlife/Slimlife-background.png') }}" alt=""> 
            <h3 class="slider-title">Slimlife</h3>
            <p class="slider-description">Cara Sehat Dalam Satu Genggaman</p>
        </div>
    </div>
</div>
<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
</a>
</div>
</div>
</div>
</div>
</div>
</section>
    <script type='text/javascript'></script>
    </body>
</html>




