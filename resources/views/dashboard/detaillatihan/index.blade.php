<h2>Detail Latihan</h2>

    @if ($user)
        <h3>{{ $user->nama_latihan }}</h3>
        <p>Waktu Latihan: {{ $user->waktu_latihan }}</p>
        <p>Kalori: {{ $user->kalori }} Kalori</p>
        <h3>Berikut Detail Gerakan</h3>
        {!! nl2br(e($user->deskripsi)) !!}
        <br><br>
        <!-- Tambahkan informasi detail latihan lainnya -->
        @if (!$user->selesai)
            <form action="{{ route('selesai.latihan', ['id' => $user->id]) }}" method="post">
                @csrf
                <button type="submit">Selesai</button>
            </form>
        @endif
    @else
        <p>Latihan tidak ditemukan.</p>
    @endif
