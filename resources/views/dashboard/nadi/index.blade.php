<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Check Up | Slimlife</title>
    
    <!-- Load jQuery from CDN -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            setInterval(function() {
                // Fetch detak jantung value and status information from the server
                $.get("{{ url('cekdetakjantung') }}", function(detakJantungResponse) { 
                    // Assuming detakJantungResponse contains the heart rate value

                    // Display the heart rate value
                    $("#cekdetakjantung").html(" " + detakJantungResponse);

                    // Check the heart rate value and update the status accordingly
                    if (detakJantungResponse == 0) {
                        $("#cekstatus").html("Tempatkan jari untuk pengukuran");
                    } else if (detakJantungResponse == 100) {
                        $("#cekstatus").html("Normal");
                    } else if (detakJantungResponse == 200) {
                        $("#cekstatus").html("Sangat Tinggi");
                    } else {
                        // Handle other cases if needed
                        $("#cekstatus").html("Segera Periksa ke Dokter");
                    }
                });
            }, 1000);
        });
    </script>
</head>
<body>
    <span id="cekdetakjantung"></span>
    
    <span id="cekstatus"></span>
</body>
</html>
