<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/x-icon" href="../assets/favicon.png">
    <title>Slimlife | Forum Diskusi</title>
    <link rel="stylesheet" href="css/edukasi.css" />
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/index.css" rel="stylesheet">
  </head>
  <body>
      <section class="courses">
        <div class="container">
          <h2 class="section__title">Forum Diskusi Slimlife</h2>
          <div class="d-flex align-content-center justify-content-center m-4">
                <a href="https://chat.whatsapp.com/B668dHnOci18Ui4HDPMH0d" type="button" class="btn bg-primary text-white"><i
                class="fa fa-paper-plane text-white fs-4"></i> Ajukan Pertanyaan</a>
          </div>
                
             
           
        
          {{-- <div class="box-container">
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/penyakit.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>7 Penyakit Mematikan yang Perlu Diwaspadai</h3>
                <p>
                  Penyakit mematikan sering kali dianggap sebagai penyakit yang berkembang dengan cepat dan tidak bisa diobati. Anggapan tersebut kurang tepat. Yang dimaksud penyakit mematikan adalah penyakit yang paling banyak menimbulkan kematian pada penderitanya.
                </p>
              </div>
            </div>
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/minuman.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>9 Minuman Sehat Penurun Berat Badan</h3>
                <p>Ada beragam minuman sehat penurun berat badan, mulai dari jus sayuran dan buah, teh hijau, hingga air putih. Minuman ini rendah gula dan kalori serta mengandung beragam nutrisi dan tinggi serat sehingga mengonsumsinya bisa menimbulkan efek kenyang dan menekan nafsu makan.</p>
              </div>
            </div>
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/karbohidrat.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>Dampak Kekurangan Karbohidrat dan Cara Mengatasinya</h3>
                <p>
                  Banyak orang menjauhi karbohidrat dengan tujuan untuk mengurangi berat badan. Padahal, kekurangan karbohidrat dapat membawa beragam gangguan kesehatan, karena karbohidrat merupakan nutrisi peting bagi tubuh. Simak beragam dampak dari kekurangan karbohidrat bagi tubuh berikut ini.
                </p>
              </div>
            </div>
          </div> --}}
        </div>
      </section>
  </body>
</html>