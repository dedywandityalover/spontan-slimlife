<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Slimlife | Form</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='' rel='stylesheet'>
        <style>



body {
background: white;
}

form {
      max-width: 400px;
      margin: auto;
    }

    label {
      margin-bottom: 8px;
    }

    input, select {
      width: 100%;
      padding: 8px;
      margin-bottom: 16px;
      box-sizing: border-box;
    }

    button {
      background-color: rgb(142, 184, 53) ;
      color: white;
      padding: 10px;
      border: none;
      cursor: pointer;
    }

a{
    text-decoration: none;
    color: rgb(142, 184, 53)
}

@media only screen and (max-width: 767px) {
.hide-on-mobile {
display: none;
}
}

.login-box {
background: url({{ asset('slimlife/background.png') }});
background-size: cover;
background-position: center;
padding: 50px;
margin: 50px auto;
min-height: 700px;
-webkit-box-shadow: 0 2px 60px -5px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 60px -5px rgba(0, 0, 0, 0.1);
}

.logo {
font-size: 54px;
text-align: center;
margin-bottom: 50px;
}

.logo .logo-font {
color: rgb(142, 184, 53);
}

@media only screen and (max-width: 767px) {
.logo {
font-size: 34px;
}
}

.header-title {
text-align: center;
margin-bottom: 50px;
}

.login-form {
max-width: 300px;
margin: 0 auto;
}

.login-form .form-control {
border-radius: 0;
margin-bottom: 30px;
}

.login-form .form-group {
position: relative;
}

.login-form .form-group .forgot-password {
position: absolute;
top: 6px;
right: 15px;
}

.login-form .btn {
border-radius: 0;
-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
margin-bottom: 30px;
}

.login-form .btn.btn-primary {
background: rgb(142, 184, 53);
border-color: rgb(142, 184, 53);
}

.slider-feature-card {
background: #fff;
max-width: 280px;
margin: 0 auto;
padding: 30px;
text-align: center;
-webkit-box-shadow: 0 2px 25px -3px rgba(0, 0, 0, 0.1);
box-shadow: 0 2px 25px -3px rgba(0, 0, 0, 0.1);
}

.slider-feature-card img {
height: 80px;
margin-top: 30px;
margin-bottom: 30px;
}

.slider-feature-card h3,
.slider-feature-card p {
margin-bottom: 30px;
}

.carousel-indicators {
bottom: -50px;
}

.carousel-indicators li {
cursor: pointer;
}</style>
        <script type='text/javascript' src=''></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'></script>
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js'></script>
    </head>
    <body oncontextmenu='return false' class='snippet-body'>
    <section class="body">
<div class="container">
<div class="login-box">
<div class="row">
<div class="col-sm-6">
<div class="logo">
<life class="logo-font">Formulir Pengguna
</div>
</div>
<div class="row">
<div class="col-sm-6">
<br>
<form action="/form" method="POST" class="signin-form">
  @csrf
<input type="hidden" name="id_user" id="" value="{{ auth()->user()->id }}">
<label for="jenis_kelamin">Jenis Kelamin:</label>
<select id="jenis_kelamin" name="jenis_kelamin">
<option value="Laki-laki">Laki-laki</option>
<option value="Perempuan">Perempuan</option>
</select>

<label for="umur">Umur:</label>
<input type="number" id="umur" name="umur" min="1" required>

<label for="tinggi_badan">Tinggi Badan (cm):</label>
<input type="number" id="height" name="tinggi_badan" min="1" required>

<label for="berat_badan_user">Berat Badan (Kg):</label>
<input type="number" id="berat_badan_user" name="berat_badan_user" min="1" required>

<label for="berat_badan_target">Berat Badan Target (kg):</label>
<input type="number" id="berat_badan_target" name="berat_badan_target" min="1" required>

<label for="ukuran_badan_user">Ukuran Badan (kg):</label>
<select  id="ukuran_badan_user" name="ukuran_badan_user" min="1" required>
  <option value="Kurus">Kurus</option>
  <option value="Gendut">Gendut</option>
</select>

<label for="ukuran_badan_target">Ukuran Badan Target (kg):</label>
<select  id="ukuran_badan_target" name="ukuran_badan_target" min="1" required>
  <option value="Kurus">Kurus</option>
  <option value="Gendut">Gendut</option>
</select>

<label for="waktu_olahraga">Waktu Olahraga ?</label>
  <input type="range" id="waktu_olahraga" name="waktu_olahraga" min="1" max="5" oninput="updateValue(this.value)">
  <span id="sliderValue">Nilai: </span>
<br>
<br>

<button type="submit">Submit</button>
</form>
</div>
</div>
</div>
</div>
</section>
    <script type='text/javascript'></script>

<script>
  function updateValue(value) {
      document.getElementById('sliderValue').innerText = 'Nilai: ' + value;
  }
</script>
    </body>
</html>










