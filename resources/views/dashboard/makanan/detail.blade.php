<h2>Detail Resep Makanan</h2>

    @if ($user)
        <h3>{{ $user->nama_makanan }}</h3>
        <p>protein: {{ $user->protein }}</p>
        <p>Karbohidrat: {{ $user->karbohidrat }}</p>
        <p>Lemak: {{ $user->lemak }}</p>
        <p>Kalori: {{ $user->kaloris }} Kalori</p>
        <h3>Berikut Resep Makanan</h3>
        {!! nl2br(e($user->resep)) !!}
        <br><br>
        <h3>Berikut Cara Memasak</h3>
        {!! nl2br(e($user->cara_masak)) !!}
        <br><br>
        <!-- Tambahkan informasi detail latihan lainnya -->
        @if (!$user->selesai)
            <form action="{{ route('selesai.makanan', ['id' => $user->id]) }}" method="post">
                @csrf
                <button type="submit">Selesai</button>
            </form>
        @endif
    @else
        <p>Resep Makan Tidak Ditemukan.</p>
    @endif