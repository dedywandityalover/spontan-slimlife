<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slimlife | Dashboard</title>
    <link rel="stylesheet" href="css/program.css">
</head>
<body>
  <!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Slimlife | Dashboard</title>
    <!-- Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="../css/dashboard.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    
<![endif]-->
</head>

<body>

   
   
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">


        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-lg">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-lg-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="/dashboard">
                            <h1 class="mt-3 ms-4" style="color:#8EB835">Slimlife</h1>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-lg-none waves-effect waves-light" href="javascript:void(0)"
                        data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left me-auto ms-3 ps-1">
                        <!-- Notification -->
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pl-md-3 position-relative" href="javascript:void(0)"
                                id="bell" role="button" data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <span><i data-feather="bell" class="svg-icon"></i></span>
                                <span class="badge notify-no rounded-circle" style="background: #8EB835">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="message-center notifications position-relative">
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <div class="btn btn-danger rounded-circle btn-circle"><i
                                                        data-feather="airplay" class="text-white"></i></div>
                                                <div class="w-75 d-inline-block v-middle ps-2">
                                                    <h6 class="message-title mb-0 mt-1">Luanch Admin</h6>
                                                    <span class="font-12 text-nowrap d-block text-muted">Just see
                                                        the my new
                                                        admin!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-success text-white rounded-circle btn-circle"><i
                                                        data-feather="calendar" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle ps-2">
                                                    <h6 class="message-title mb-0 mt-1">Event today</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Just
                                                        a reminder that you have event</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-info rounded-circle btn-circle"><i
                                                        data-feather="settings" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle ps-2">
                                                    <h6 class="message-title mb-0 mt-1">Settings</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">You
                                                        can customize this template
                                                        as you want</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-primary rounded-circle btn-circle"><i
                                                        data-feather="box" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle ps-2">
                                                    <h6 class="message-title mb-0 mt-1">Pavan kumar</h6> <span
                                                        class="font-12 text-nowrap d-block text-muted">Just
                                                        see the my admin!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link pt-3 text-center text-dark" href="javascript:void(0);">
                                            <strong>Check all notifications</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li> --}}
                        <!-- End Notification -->
                        <!-- ============================================================== -->
                     
                        {{-- <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <div class="customize-input">
                                    <select
                                        class="custom-select form-control bg-white custom-radius custom-shadow border-0">
                                        <option selected>Indonesia</option>
                                        <option value="1">English</option> 
                                    </select>
                                </div>
                            </a>
                        </li> --}}
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        {{-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="assets/images/users/profile-pic.jpg" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ms-2 d-none d-lg-inline-block"><span>Hello,</span> <span
                                        class="text-dark">Jason Doe</span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="user"
                                        class="svg-icon me-2 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="power"
                                        class="svg-icon me-2 ms-1"></i>
                                    Logout</a>
                        </li> --}}
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/dashboard"
                                aria-expanded="false"><i class="fa-solid fa-house" style="font-size:20px; color:black"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/makanan"
                                aria-expanded="false"><i class="fa-solid fa-bowl-food" style="font-size:20px; color:black"></i><span
                                    class="hide-menu">Pemantauan Gizi</span></a></li>
                        <li class="list-divider"></li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/toko"
                                aria-expanded="false"><i class="fa-solid fa-cart-shopping"></i><span
                                    class="hide-menu">Toko</span></a></li>
                                    <li class="list-divider"></li>
                        {{-- <li class="nav-small-cap"><span class="hide-menu">Applications</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="ticket-list.html"
                                aria-expanded="false"><i data-feather="tag" class="feather-icon"></i><span
                                    class="hide-menu">Ticket List
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="/logout"
                          aria-expanded="false"><i data-feather="crosshair" class="feather-icon"></i><span
                              class="hide-menu">Multi
                              level
                              dd</span></a>
                      <ul aria-expanded="false" class="collapse first-level base-level-line">
                          <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><span
                                      class="hide-menu"> item 1.1</span></a>
                          </li>
                          <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><span
                                      class="hide-menu"> item 1.2</span></a>
                          </li>
                          
                          <li class="sidebar-item"><a href="javascript:void(0)" class="sidebar-link"><span
                                      class="hide-menu"> item
                                      1.4</span></a></li>
                      </ul>
                  </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="app-calendar.html"
                                aria-expanded="false"><i data-feather="calendar" class="feather-icon"></i><span
                                    class="hide-menu">Calendar</span></a></li>
 
                        <li class="list-divider"></li> --}}
                        
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="/logout"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Logout</span></a></li>
                                    
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            @if(session('success'))
            <div class="page-content page-container" id="page-content">
                <div class="padding">
                    <div class="row d-flex justify-content-center"> <button type="button" id="successtoast" class="btn  btn-icon-text text-white" style="background: #8EB835"> <i class="fa fa-check btn-icon-prepend"></i> {{ 'Sukses' }}</button> </div>
                </div>
            @endif
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Selamat Datang {{ auth()->user()->name }}</h3>  
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    {{-- <div class="col-5 align-self-center">
                        <div class="customize-input float-end">
                            <select class="custom-select custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
                                <option selected>Aug 23</option>
                                <option value="1">July 23</option>
                                <option value="2">Jun 23</option>
                            </select>
                        </div>
                    </div> --}}
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container">
                <!-- *************************************************************** -->
                <!-- Start First Cards -->
                <!-- *************************************************************** -->
                <div class="row mt-2">
                    <a href="/form" class="col-sm-6 col-lg-3 ">
                        <div class="card border-end">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <div class="d-inline-flex align-items-center">
                                            <h2 class="text-dark mb-1 font-weight-medium">Program <br> Diet</h2>
                                        </div>
                                    </div>
                                    <div class="ms-auto mt-md-3 mt-lg-0">
                                        <span class="opacity-7 text-muted"><i class="fa-solid fa-plus" style="font-size:25px"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/makanan" class="col-sm-6 col-lg-3">
                        <div class="card border-end ">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h2 class="text-dark mb-1 font-weight-medium">Pemantauan Gizi</h2>
                                    </div>
                                    <div class="ms-auto mt-md-3 mt-lg-0">
                                        <span class="opacity-7 text-muted"><i class="fa-solid fa-bowl-food" style="font-size:25px"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="https://chat.whatsapp.com/B668dHnOci18Ui4HDPMH0d" class="col-sm-6 col-lg-3">
                        <div class="card border-end ">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <div class="d-inline-flex align-items-center">
                                            <h2 class="text-dark mb-1 font-weight-medium">Forum Diskusi</h2>
                                        </div>
                                    </div>
                                    <div class="ms-auto mt-md-3 mt-lg-0">
                                        <span class="opacity-7 text-muted"><i class="fa-solid fa-comment" style="font-size:25px"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="/edukasi" class="col-sm-6 col-lg-3">
                        <div class="card ">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h2 class="text-dark mb-1 font-weight-medium">Slimlife Edukasi</h2>
                                    </div>
                                    <div class="ms-auto mt-md-3 mt-lg-0">
                                        <span class="opacity-7 text-muted"><i class="fa-solid fa-book-open-reader" style="font-size:25px"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>



                {{-- Progress bar --}}
                  <!-- ============================================================== -->
                  <!-- Start Page Content -->
                  <!-- ============================================================== -->
                  <div class="row">
                      <div class="col-12">
                          <div class="card">
                              <div class="card-body">
                                  <div class="row">
                                      <div class="col-md-11">
                                        
                                          <h4 class="card-title">Progress</h4>
                                          @if (auth()->user()->program->count() > 0)
                                      
                                        @php
                                            $totalLatihan = auth()->user()->program->count();
                                            $latihanSelesai = auth()->user()->program->where('selesai', true)->count();
                                            $persentaseSelesai = round(($latihanSelesai / $totalLatihan) * 100);
                                        @endphp
  
                                          <p class="text-muted mb-1 font-14">
                                              Progress Olahraga
                                          </p>
  
                                          <div class="progress">
                                              <div class="progress-bar" role="progressbar" style="width: {{ $persentaseSelesai }}%"
                                                  aria-valuenow="{{ $persentaseSelesai }}" aria-valuemin="0" aria-valuemax="100"> <span>{{ $persentaseSelesai }}% </span></div>
                                          </div>
                                      </div>  
                                      @endif

                                      <div class="col-md-11 mt-4">
                                        @if (auth()->user()->makanan->count() > 0)
                                      
                                        @php
                                            $totalLatihan = auth()->user()->makanan->count();
                                            $latihanSelesai = auth()->user()->makanan->where('selesai', true)->count();
                                            $persentaseSelesai = round(($latihanSelesai / $totalLatihan) * 100);
                                        @endphp
                                          <p class="text-muted mb-1 font-14">
                                              Progress Gizi
                                          </p>
  
                                          <div class="progress">
                                              <div class="progress-bar" role="progressbar" style="width: {{ $persentaseSelesai }}%"
                                                  aria-valuenow="{{ $persentaseSelesai }}" aria-valuemin="0" aria-valuemax="100"> <span>{{ $persentaseSelesai }}% </span></div>
                                          </div>
                                      </div>  
                                      @endif
                                  </div>
                                  <!-- end row -->
  
                              </div> <!-- end card-body-->
                          </div> <!-- end card -->
                      </div> <!-- end col -->
                  </div>
                  <!-- end row -->
                  <!-- ============================================================== -->
                  <!-- End PAge Content -->
                  <!-- ============================================================== -->
              {{-- end progress bar --}}



                <!-- *************************************************************** -->
                <!-- End First Cards -->
                <!-- *************************************************************** -->
                <!-- *************************************************************** -->
               
               
                <!-- Start Location and Earnings Charts Section -->
                <!-- *************************************************************** -->
                <div class="row">
                    <div class="col-md-6 col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="pl-4 mb-2">
                                  <div class="col-md-12">
                                    <h4 class="card-title">Target Kurangin Berat Badan</h4>

                                    <h2 class="text-mute font-weight-medium mt-3 mr-5 d-flex justify-content-center">
                                        @if(auth()->user()->form)
                                        {{ auth()->user()->form->latest('id')->value('berat_badan_target') }}.0 Kg | 
                                        @if(auth()->user()->form->latest('id')->value('berat_badan_target') && auth()->user()->form->latest('id')->value('berat_badan_user'))
                                            {{ abs(auth()->user()->form->latest('id')->value('berat_badan_user') - auth()->user()->form->latest('id')->value('berat_badan_target')) }}.0 Kg
                                        @else
                                            Data belum terisi
                                        @endif
                                    @else
                                        Data belum terisi
                                    @endif
                                    </h2>
                                    <div class="d-flex justify-content-center mt-3">
                                        <button type="button" class="btn waves-effect waves-light btn-rounded text-white " style="background: #8EB835" data-bs-toggle="modal" data-bs-target="#exampleModal">Catat</button>
                                    </div>
                                  </div>  
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- modal Update berat badan user --}}


                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="updateWeightModalLabel">Update Berat Badan User</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{ route('update.berat') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="newWeight">Berat Badan Baru (Kg):</label>
                                            @if(auth()->user()->form)
                                            <input type="number" class="form-control" id="newWeight" name="berat_badan_user" value="{{ auth()->user()->form->latest('id')->value('berat_badan_user') }}">
                                            @else
                                            Data Belum terisi
                                            @endif
                                        </div>
                                        <button type="submit" class="btn text-white mt-3" style="background: #8EB835">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- akhir modal --}}

                    <a href="/nadi" class="col-md-6 col-lg-4" onclick="showUnderDevelopmentMessage(event)">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Fitur Slimlife</h4>
                                <div class="mt-5 activity">
                                    <div class="d-flex align-items-start border-left-line pb-3">
                                        <div>
                                            <div class="btn btn-info btn-circle btn-item">
                                                <i data-feather="heart"></i>
                                            </div>
                                        </div>
                                        <div class="ms-2  ">
                                            <h5 class="text-dark font-weight-medium ">Check Up Denyut <br> Nadi</h5>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- *************************************************************** -->
                <!-- End Location and Earnings Charts Section -->
                <!-- *************************************************************** -->
                <!-- *************************************************************** -->
                <!-- Start Top Leader Table -->
                <!-- *************************************************************** -->
                <div class="row" style="width: 103%;">
                    <div class="col-13">
                        <div class="card">
                            <div class="card-body">
                                <div class=" mb-1 ms-2"  >
                                    <h4 class="card-title">Program Latihan</h4>
                                    <p>Berikut adalah program latihan berdasarkan data yang Anda masukkan:</p>

                                    @if (auth()->user()->program->count() > 0)
  <div class="ag-format-container">
    @foreach (auth()->user()->program as $user)
      <div class="ag-courses_box">
        <div class="ag-courses_item">
          <a href="{{ route('detail.latihan', ['id' => $user->id]) }}" class="ag-courses-item_link">
            <div class="ag-courses-item_bg"></div>
            <div class="ag-courses-item_title">
              <h4>{{ $user->nama_latihan }}</h4> 
              @if ($user->selesai)
            <h5 class="checklist">(Selesai)</h5>
            @endif
            </div>
            <div class="ag-courses-item_date-box">
              <span class="ag-courses-item_date">
                {{ $user->waktu_latihan }} | {{ $user->kalori }} Kalori
              </span>
            </div>
          </a>
        </div>
      </div>
    @endforeach
  </div>
                                @else
                                    <p>Tidak ada program latihan yang di-generate. <a href="/form">Buat Program Latihan di Klik Sini</a></p>
                                @endif
                                   
                                
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- *************************************************************** -->
                <!-- End Top Leader Table -->
                <!-- *************************************************************** -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="../js/app-style-switcher.js"></script>
    <script src="../js/feather.min.js"></script>
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!--This page JavaScript -->
    <script src="../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../assets/extra-libs/c3/c3.min.js"></script>
    <script src="../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="../assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="../assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    <script src="../js/pages/dashboards/dashboard1.min.js"></script>

    <script>
        function showUnderDevelopmentMessage(event) {
            event.preventDefault();
            alert("Maaf, fitur ini masih dalam pengembangan.");
        }
    </script>

</body>
</html>