<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slimlife | Edukasi</title>
    <link rel="stylesheet" href="{{ asset('css/edukasi.css') }}">
</head>
<body>
    <section class="blog-posts grid-system">
        <div class="container">
          <div class="blog-post">
            <div class="blog-thumb">
              <img src="{{ asset('slimlife/karbohidrat.jpg') }}" alt="" />
            </div>
            <div class="down-content">
              <h4>Dampak Kekurangan Karbohidrat dan Cara Mengatasinya</h4>
              <p>
                Banyak orang menjauhi karbohidrat dengan tujuan untuk mengurangi berat badan. Padahal, kekurangan karbohidrat dapat membawa beragam gangguan kesehatan, karena karbohidrat merupakan nutrisi peting bagi tubuh. Simak beragam dampak dari kekurangan karbohidrat bagi tubuh berikut ini.
                <br><br>
                Manfaat utama karbohidrat bagi tubuh adalah menyediakan energi untuk beraktivitas, mulai dari bernapas hingga berjalan. Selain itu, karbohidrat juga berfungsi mencegah serangan penyakit dan menjaga berat badan.
                <br><br>
                Kementerian Kesehatan Republik Indonesia menyarankan untuk mengonsumsi karbohidrat antara 350–390 gram per hari bagi pria, dan 300–320 gram per hari bagi wanita.
                <br><br>
                Berbagai jenis karbohidrat, terutama karbohidrat kompleks, dapat berasal dari nasi dan gandum, buah, sayur, serta kacang-kacangan. Kekurangan karbohidrat bisa terjadi jika Anda membatasi asupan karbohidrat atau melakuan diet rendah karbohidrat untuk menurunkan berat badan.
                <br><br>
                <b>Efek Kekurangan Karbohidrat</b>
                <br><br>
                Kekurangan karbohidrat dapat menyebabkan berbagai gangguan kesehatan, mulai dari kepala pusing, tubuh terasa lemah, hingga rentan terserang penyakit. Dampak kekurangan karbohidrat bisa terjadi dalam jangka pendek maupun jangka panjang. Berikut ini adalah penjelasannya:
                <br><br>
                <b>Jangka pendek</b>
                <br><br>
                Dalam jangka pendek, kekurangan karbohidrat dapat menyebabkan ketosis, yaitu suatu kondisi ketika tubuh memanfaatkan lemak sebagai sumber energi. Gejala ketosis antara lain adalah sakit kepala, lemas, dehidrasi, mual, pusing, dan mudah emosi.

                Ketosis dapat menyebabkan penumpukan senyawa keton dalam tubuh. Dalam jangka panjang, keton yang menumpuk ini bisa menyebabkan gangguan kesehatan serius, seperti asidosis, koma, bahkan kematian.
                <br><br>
                <b>Jangka panjang</b>
                <br><br>
                Kekurangan karbohidrat secara terus-menerus juga akan menimbulkan dampak jangka panjang terhadap kesehatan, di antaranya:
                <br><br>


              </p>
              <div class="post-options">
                <ul class="post-share">
                  <li>Slimlife</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
</body>
</html>