<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/x-icon" href="../assets/favicon.png">
    <title>Slimlife | Edukasi</title>
    <link rel="stylesheet" href="css/edukasi.css" />
  </head>
  <body>
    

      <section class="courses">
        <div class="container">
          <h2 class="section__title">Edukasi Slimlife</h2>
          <div class="box-container">
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/penyakit.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>7 Penyakit Mematikan yang Perlu Diwaspadai</h3>
                <p>
                  Penyakit mematikan sering kali dianggap sebagai penyakit yang berkembang dengan cepat dan tidak bisa diobati. Anggapan tersebut kurang tepat. Yang dimaksud penyakit mematikan adalah penyakit yang paling banyak menimbulkan kematian pada penderitanya.
                </p>
                <div class="icons">
                  <span>956 Dilihat</span>
                  <a href="/edukasi1" class="button"><span>Selengkapnya</span></a>
                </div>
              </div>
            </div>
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/minuman.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>9 Minuman Sehat Penurun Berat Badan</h3>
                <p>Ada beragam minuman sehat penurun berat badan, mulai dari jus sayuran dan buah, teh hijau, hingga air putih. Minuman ini rendah gula dan kalori serta mengandung beragam nutrisi dan tinggi serat sehingga mengonsumsinya bisa menimbulkan efek kenyang dan menekan nafsu makan.</p>
                <div class="icons">
                  <span>535 Dilihat</span>
                  <a href="/edukasi2" class="button"><span>Selengkapnya</span></a>
                </div>
              </div>
            </div>
            <div class="box">
              <div class="image shine">
                <img src="{{ asset('slimlife/karbohidrat.jpg') }}" alt="" />
              </div>
              <div class="content">
                <h3>Dampak Kekurangan Karbohidrat dan Cara Mengatasinya</h3>
                <p>
                  Banyak orang menjauhi karbohidrat dengan tujuan untuk mengurangi berat badan. Padahal, kekurangan karbohidrat dapat membawa beragam gangguan kesehatan, karena karbohidrat merupakan nutrisi peting bagi tubuh. Simak beragam dampak dari kekurangan karbohidrat bagi tubuh berikut ini.
                </p>
                <div class="icons">
                  <span>1069 Dilihat</span>
                  <a href="/edukasi3" class="button"><span>Selengkapnya</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
  </body>
</html>