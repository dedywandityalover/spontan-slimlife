<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slimlife | Edukasi</title>
    <link rel="stylesheet" href="{{ asset('css/edukasi.css') }}">
</head>
<body>
    <section class="blog-posts grid-system">
        <div class="container">
          <div class="blog-post">
            <div class="blog-thumb">
              <img src="{{ asset('slimlife/minuman.jpg') }}" alt="" />
            </div>
            <div class="down-content">
              <h4>9 Minuman Sehat Penurun Berat Badan</h4>
              <p>
                Ada beragam minuman sehat penurun berat badan, mulai dari jus sayuran dan buah, teh hijau, hingga air putih. Minuman ini rendah gula dan kalori serta mengandung beragam nutrisi dan tinggi serat sehingga mengonsumsinya bisa menimbulkan efek kenyang dan menekan nafsu makan.
                <br><br>
                Konsumsi makanan tinggi serat dan olahraga secara rutin kerap dilakukan untuk mendapatkan berat badan ideal. Namun, tidak hanya dari makanan dan aktivitas fisik, Anda pun bisa mengonsumsi minuman sehat penurun berat badan.
                <br><br>
                <b>Ragam Pilihan Minuman Sehat Penurun Berat Badan</b>
                <br><br>
                Berikut ini adalah berbagai pilihan minuman sehat penurun berat badan:
                <br><br>
                <b>1. Air putih</b>
                Tak hanya menyehatkan, air putih merupakan minuman sehat yang paling praktis dan paling mudah dikonsumsi. Sebagai minuman sehat, air putih tidak mengandung kalori maupun gula yang berisiko menambah berat badan.
                <br><br>
                Sebuah penelitian menyatakan bahwa minum air putih sebanyak 1,5 liter setiap harinya selama 8 minggu dapat menurunkan berat badan. Minum air putih juga dapat meningkatkan pembakaran kalori di dalam tubuh.
                <br><br>
                Untuk mendapatkan manfaat air putih sebagai minuman sehat penurun berat badan, Anda disarankan minum 2 gelas air putih sebelum makan agar memberikan efek kenyang dan mencegah Anda makan dengan porsi berlebihan. Jika bosan dengan rasa tawar dari air putih, Anda bisa menambahkan irisan lemon atau membuat infused water.
                <br><br>

                <b>2. Jus sayuran</b>
                Jus sayuran bisa dijadikan sebagai minuman sehat penurun berat badan. Jus ini memiliki gizi yang sama dengan jus buah. Bedanya, jus sayuran mengandung lebih sedikit kalori bila daripada jus buah. Dalam secangkir jus tomat terkandung 40 kalori, sedangkan secangkir jus jeruk memiliki 120 kalori.

Selain itu, jus sayuran juga banyak mengandung serat sehingga bisa membantu Anda mengendalikan rasa lapar.
                <br><br>
                <b>3. Jus buah</b>
                Meskipun memiliki kalori lebih tinggi dari jus sayuran, jus buah tetap bisa menjadi minuman sehat untuk menurunkan berat badan. Namun, hindari mengonsumsi jus buah kemasan yang biasanya telah ditambahkan gula atau pemanis buatan dan pengawet.

                Anda dianjurkan untuk mengolah sendiri buah untuk dijadikan jus menggunakan blender atau juicer. Pastikan juga jus buah yang Anda konsumsi tidak ditambahkan gula atau susu kental manis.
                <br><br>
                <b>4. Susu kedelai</b>
                Susu kedelai juga termasuk minuman sehat penurun berat badan. Susu ini rendah kalori dan tinggi nutrisi. Anda disarankan mengonsumsi susu kedelai rendah lemak yang diperkaya vitamin D serta kalsium.

                Susu kedelai juga bisa menjadi pilihan bagi Anda yang memiliki kondisi intoleransi laktosa. Tak hanya itu, susu kedelai dapat menurunkan risiko terjadinya osteoporosis dan penyakit jantung.
                <br><br>
                <b>5. Susu rendah lemak</b>
                Keunggulan susu rendah lemak adalah kandungan lemaknya yang lebih sedikit daripada susu murni. Oleh karena itu, susu rendah lemak bisa Anda pilih sebagai minuman sehat untuk menurunkan berat badan.
                <br><br>
                <b>6. Teh hijau</b>
                Teh hijau merupakan minuman sehat bebas kalori dan kaya akan kandungan antioksidan. Selain dapat menurunkan berat badan, kandungan antioksidan di dalam teh hijau juga dapat mengurangi risiko berbagai penyakit, seperti diabetes tipe 2, penyakit liver, dan penyakit jantung.

Untuk mendapatkan manfaat dari teh hijau tersebut, Anda disarankan mengonsumsinya 2 kali sehari. Jika Anda lebih menyukai teh hijau yang manis, hindari menambahkan gula dan gunakan madu sebagai gantinya.
                <br><br>
                <b>7. Kopi</b>
                Minuman sehat penurun berat badan selanjutnya yang bisa dicoba adalah kopi. Anda disarankan mengonsumsi kopi tanpa pemanis atau susu dalam bentuk apa pun. Meski demikian, Anda tidak disarankan minum kopi secara berlebihan, cukup dua cangkir setiap hari.

                Kopi bebas kalori dan kaya akan antioksidan. Selain menurunkan berat badan, minum kopi juga bisa meningkatkan suasana hati dan konsentrasi, serta menurunkan risiko terkena diabetes tipe 2.
                <br><br>
                <b>8. Smoothies</b>
                Membuat smoothies dari campuran buah-buahan, seperti stroberi, pisang, dan blueberry bisa menjadi pilihan minuman sehat penurun berat badan. Anda bisa menambahkan susu rendah lemak di dalam smoothies agar terlihat lebih kental.

Hindari mengonsumsi smoothies dalam kemasan karena biasanya telah ditambahkan pemanis dan es krim yang membuatnya tinggi kalori.
                <br><br>
                <b>9. Air kelapa</b>
                Minuman sehat penurun berat badan yang juga bisa Anda coba adalah air kelapa. Dalam takaran sekitar 250 ml, minuman pelepas dahaga ini hanya mengandung 60 kalori.

Selain termasuk minuman rendah kalori, air kelapa juga dilengkapi dengan elektrolit dan kandungan antioksidan yang baik untuk kesehatan. Namun, ingat, pastikan Anda mengonsumsinya secara utuh tanpa tambahan gula.


                <br><br>
                Berbagai jenis minuman sehat di atas dapat dikonsumsi untuk menurunkan berat badan dan menjaga berat badan tetap ideal. Namun, sertai pula dengan konsumsi makanan bernutrisi dan berolahraga secara rutin.

Jika sudah mulai rutin mengonsumsi minuman sehat penurun berat badan tetapi tidak cukup membantu, Anda dapat berkonsultasi dengan dokter guna mendapatkan saran mengenai diet yang sesuai untuk Anda.

              </p>
              <div class="post-options">
                <ul class="post-share">
                  <li>Slimlife</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
</body>
</html>