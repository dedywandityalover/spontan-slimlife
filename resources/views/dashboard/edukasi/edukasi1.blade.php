<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slimlife | Edukasi</title>
    <link rel="stylesheet" href="{{ asset('css/edukasi.css') }}">
</head>
<body>
    <section class="blog-posts grid-system">
        <div class="container">
          <div class="blog-post">
            <div class="blog-thumb">
              <img src="{{ asset('slimlife/penyakit.jpg') }}" alt="" />
            </div>
            <div class="down-content">
              <h4>Peran Teknologi Virtual Reality dalam Pembelajaran</h4>
              <p>
                Penyakit mematikan adalah beragam kondisi kesehatan yang dapat mengancam nyawa. Dengan mengetahui apa saja yang termasuk penyakit mematikan, langkah-langkah pencegahan dapat dilakukan.
                <br><br>
                Penyakit mematikan sering kali dianggap sebagai penyakit yang berkembang dengan cepat dan tidak bisa diobati. Anggapan tersebut kurang tepat. Yang dimaksud penyakit mematikan adalah penyakit yang paling banyak menimbulkan kematian pada penderitanya.
                <br><br>
                Dengan makin majunya dunia medis, metode pengobatan untuk penyakit mematikan juga makin berkembang. Ini membuat peluang penderitanya untuk tetap hidup pun makin besar jika kondisi yang diderita segera diketahui dan ditangani dengan tepat.
                <br><br>
                <b>Jenis-Jenis Penyakit Mematikan</b>
                Ada penyakit yang dapat menyebabkan kematian karena organ tubuh yang diserangnya mengalami kerusakan atau kehilangan fungsi secara total. Ada pula penyakit yang menyebabkan kematian karena komplikasinya.
                Berikut ini adalah daftar penyakit mematikan yang paling umum:
                <br><br>
                <b>1. Penyakit jantung koroner</b>
                Penyakit ini menjadi penyakit paling mematikan di dunia berdasarkan jumlah kematian penderitanya. Penyakit jantung koroner disebabkan oleh penyempitan dan penyumbatan yang terjadi pada pembuluh darah yang memasok darah dan oksigen ke jantung.

                Ketika jantung sama sekali tidak mendapat pasokan oksigen dan darah, organ ini akan berhenti berfungsi.
                <br><br>
                <b>2. Stroke</b>
                Penyakit mematikan kedua adalah stroke. Stroke terjadi ketika pembuluh arteri di otak tersumbat atau pecah, sehingga memutus pasokan darah dan oksigen ke organ ini.

                    Kondisi ini menyebabkan area tertentu pada otak tidak mendapat suplai oksigen dan nutrisi, sehingga terjadi kematian sel-sel otak. Akibatnya, bagian tubuh yang dikendalikan oleh area otak tersebut tidak bisa berfungsi dengan baik.

                    Jika tidak ditangani, stroke dapat menyebabkan kecacatan jangka panjang.
                <br><br>
                <b>3. Diabetes</b>
                Di Indonesia, diabetes menjadi penyakit mematikan ketiga setelah penyakit jantung dan stroke. Diabetes adalah penyakit yang ditandai dengan kadar gula darah yang tinggi dalam jangka waktu panjang.

                Pada diabetes tipe 1, pankreas tidak bisa memproduksi insulin dalam jumlah yang cukup, biasanya akibat diserang oleh sistem kekebalan tubuhnya sendiri (autoimun). Sementara pada diabetes tipe 2, sel-sel tubuh tidak peka terhadap insulin, sehingga fungsi insulin dalam menurunkan kadar gula darah tidak efektif meskipun jumlahnya cukup.

                Fungsi insulin adalah untuk mengubah gula menjadi energi. Ketika insulin tidak cukup jumlahnya atau tidak dapat digunakan secara efektif, zat gula akan menumpuk di dalam darah. Akibatnya, penderita diabetes dapat mengalami komplikasi yang dapat membahayakan nyawa, seperti gagal ginjal dan penyakit jantung.
                <br><br>
                <b>4. Kanker</b>
                Kanker mungkin menjadi momok yang menakutkan bagi sebagian orang. Penyakit mematikan ini terjadi ketika sel-sel di dalam tubuh mengalami kelainan atau mutasi, sehingga berkembang secara tidak terkendali.

                    Ketika sel-sel kanker sudah menyebar ke berbagai organ tubuh (metastasis), penderitanya dapat mengalami kematian. Contoh jenis kanker yang paling banyak menyebabkan kematian adalah kanker paru-paru, otak, darah, usus besar, dan payudara.

                    Di Indonesia, kanker payudara menempati urutan pertama sebagai jenis kanker yang paling banyak diderita, serta menjadi salah satu penyumbang kematian terbesar akibat kanker.
                <br><br>
                <b>5. HIV/AIDS</b>
                Human immunodeficiency virus (HIV) adalah virus yang menyerang sistem kekebalan tubuh. HIV dapat menyebabkan AIDS (acquired immunodeficiency syndrome), yaitu kondisi ketika tubuh tidak mampu lagi melawan infeksi.

                Meski sampai saat ini belum ada obat untuk menyembuhkan HIV, ada jenis obat yang dapat menghambat perkembangan virus. Jika penderitanya berobat secara rutin seumur hidup, jumlah virus HIV bisa dikendalikan dan pasien HIV bisa menjalankan aktivitas sehari-harinya dengan normal.

                Menunda atau bahkan tidak mendapatkan pengobatan dapat membuat virus terus merusak sistem kekebalan tubuh dan meningkatkan risiko berkembangnya infeksi menjadi AIDS yang merupakan penyakit mematikan.
                <br><br>
                <b>6. Tuberkulosis (TBC)</b>
                TBC disebabkan oleh infeksi bakteri Mycobacterium tuberculosis yang menyerang paru-paru. Penyakit ini mudah menyebar melalui liur atau dahak yang dikeluarkan oleh penderita saat bersin maupun batuk.

                Penyakit ini berbahaya pada pasien yang tidak berobat hingga tuntas, bahkan tidak berobat sama sekali. Oleh karena itu, penderita TBC perlu minum obat antituberkulosis secara rutin selama minimal 6 bulan meskipun gejala telah hilang.
                <br><br>
                <b>7. Penyakit paru obstruktif kronis (PPOK)</b>
                Penyakit paru obstruktif kronis (PPOK) adalah kelompok penyakit pada paru-paru yang terjadi dalam jangka panjang, misalnya bronkitis kronis dan emfisema. Penyebab utama dari PPOK adalah merokok maupun terpapar asap rokok dan polusi udara dalam jangka waktu yang panjang.

Jika tidak diobati dengan tepat, PPOK dapat menimbulkan berbagai komplikasi berbahaya, seperti pneumonia dan pneumothorax.
                <br><br>
                Dengan mengetahui apa saja yang termasuk penyakit mematikan, Anda dapat melakukan langkah-langkah pencegahannya, termasuk dengan menerapkan pola hidup sehat. Anda bisa menerapkan pola hidup sehat dengan mengonsumsi makanan bergizi seimbang, menjaga berat badan ideal, rutin berolahraga, tidur yang cukup, dan tidak merokok.

              </p>
              <div class="post-options">
                <ul class="post-share">
                  <li>Slimlife</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
</body>
</html>