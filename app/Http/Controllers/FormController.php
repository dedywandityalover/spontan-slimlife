<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Program;
use App\Models\makanan;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.form.index');
    }

    
    /**
     * Store a newly created resource in storage.
     */
    public function tambah(Request $request)
    {

        $validatedData = $request->validate([
           
            'id_user' => 'required',
            'jenis_kelamin' => 'required',
            'umur' => 'required',
            'tinggi_badan' => 'required',
            'berat_badan_user' => 'required',
            'berat_badan_target' => 'required',
            'ukuran_badan_user' => 'required',
            'ukuran_badan_target' => 'required',
            'waktu_olahraga' => 'required',
        ]);

        $form = Form::create($validatedData);

        $programLatihan = $this->generateProgramLatihan($form, $validatedData);

        return redirect()->route('dashboard.index')->with('success', 'Formulir berhasil ditambahkan!');
    
    }

    

    

    private function generateProgramLatihan($form, $validatedData)
    {
        $programLatihan = [];

        if ($validatedData['jenis_kelamin'] == 'Laki-laki') {
            $programLatihan[] = ['kategori' => 'Laki-laki', 'nama_latihan' => 'Latihan Kekuatan', 'waktu_latihan' => '30 menit', 'kalori' => '200', 'deskripsi' => '1. Posisikan tubuh berdiri tegak, lalu buka kedua kaki Anda selebar pinggul.
            2. Kemudian turunkan tubuh sejauh yang Anda bisa dengan mendorong punggung ke belakang. Lakukan gerakan ini sambil meluruskan lengan Anda ke depan untuk menjaga keseimbangan.
            3. Saat berada dalam posisi setengah jongkok, pastikan tubuh bagian bawah Anda sejajar dengan lantai dan dada harus dibusungkan tapi tidak membungkuk. 
            4. Angkat tubuh kembali ke posisi awal dan ulangi gerakan 8-12 kali dalam 1-3 set.', 'selesai' => false, 'nama_makanan' => 'Roti atau sereal gandum utuh', 'protein' => '5', 'karbohidrat' => '25', 'lemak' => '0', 'kaloris' => '135', 'resep' => '1. 5 sdm Tepung terigu
            2. 1 sdm Tepung maizena
            3. secukupnya Blue band
            4. 1 sdm Gula pasir
            5. 1 sachet Energen coklat', 'cara_masak' => '1. Masukkan tepung terigu tepung maizena dan blue band secukupnya sampai bisa dibuat adonan
            2. Lalu masukkan energen dan gula pasir dan meses uleni sampai bisa dibuat adonan
            3. Bentuk bulat bulat pipihkan dan tata di teflon.
            4. Panggang dengan api sangat kecil. Tutup jika bagian bawah sudah mulai agak keras balik sebentar. Jika sudah mulai keras merata angkat.'];
        } else {
            $programLatihan[] = ['kategori' => 'Perempuan', 'nama_latihan' => 'Cardio', 'waktu_latihan' => '20 menit', 'kalori' => '150', 'deskripsi' => '1. Mulai pemanasan dengan jalan di tempat, kemudian ayunkan tangan sambil berjalan ke depan dan ke belakang secara bergantian selama 3 menit.
            2. Kepalkan tangan dan rentangkan ke depan, lalu langkahkan kaki kanan ke depan dengan bagian tumit sebagai tumpuan selama 2 detik. Lakukan gerakan ini secara bergantian dengan kaki kiri. Ulangi gerakan 30 kali.
            3. Dalam posisi berdiri tegak, angkat lutut kanan hingga membentuk sudut 90° dan sentuh lutut dengan telapak tangan kiri. Lakukan hal yang sama secara bergantian dengan lutut kiri dan ulangi sebanyak 20 kali selama 30 detik.
            4. Sambil jalan di tempat, putar bahu ke arah depan dan belakang, masing-masing 5 kali.
            5. Terakhir, berdiri tegak dengan kaki sedikit terbuka dan tangan lurus ke depan. Kemudian tekuk lutut lalu berdiri. Ulangi gerakan ini sebanyak 10 kali.', 'selesai' => false, 'nama_makanan' => 'Roti atau sereal gandum utuh', 'protein' => '5', 'karbohidrat' => '25', 'lemak' => '0', 'kaloris' => '135', 'resep' => '1. 5 sdm Tepung terigu
            2. 1 sdm Tepung maizena
            3. secukupnya Blue band
            4. 1 sdm Gula pasir
            5. 1 sachet Energen coklat', 'cara_masak' => '1. Masukkan tepung terigu tepung maizena dan blue band secukupnya sampai bisa dibuat adonan
            2. Lalu masukkan energen dan gula pasir dan meses uleni sampai bisa dibuat adonan
            3. Bentuk bulat bulat pipihkan dan tata di teflon.
            4. Panggang dengan api sangat kecil. Tutup jika bagian bawah sudah mulai agak keras balik sebentar. Jika sudah mulai keras merata angkat.' ];
        }

        if ($validatedData['umur'] <= 30) {
            $programLatihan[] = ['kategori' => 'Umur <= 30', 'nama_latihan' => 'Cardio', 'waktu_latihan' => '30 menit', 'kalori' => '200', 'deskripsi' => '1. Mulai pemanasan dengan jalan di tempat, kemudian ayunkan tangan sambil berjalan ke depan dan ke belakang secara bergantian selama 3 menit.
            2. Kepalkan tangan dan rentangkan ke depan, lalu langkahkan kaki kanan ke depan dengan bagian tumit sebagai tumpuan selama 2 detik. Lakukan gerakan ini secara bergantian dengan kaki kiri. Ulangi gerakan 30 kali.
            3. Dalam posisi berdiri tegak, angkat lutut kanan hingga membentuk sudut 90° dan sentuh lutut dengan telapak tangan kiri. Lakukan hal yang sama secara bergantian dengan lutut kiri dan ulangi sebanyak 20 kali selama 30 detik.
            4. Sambil jalan di tempat, putar bahu ke arah depan dan belakang, masing-masing 5 kali.
            5. Terakhir, berdiri tegak dengan kaki sedikit terbuka dan tangan lurus ke depan. Kemudian tekuk lutut lalu berdiri. Ulangi gerakan ini sebanyak 10 kali.',  'selesai' => false, 'nama_makanan' => 'Nasi goreng oatmeal (gandum utuh)', 'protein' => '7,7', 'karbohidrat' => '53,09', 'lemak' => '12,31', 'kaloris' => '336', 'resep' => '1. 5 sdm gandum utuh
            2. 100 ml air ±
            3. 1 sdt kecap
            4. 1/4 sdt kaldu jamur
            5. secukupnya garam
            6. secukupnya lada
            7. 1 butir telur
            8. terasi sedikit (optional)', 'cara_masak' => '1. Panaskan teflon, masukan gandum utuh sangrai sebentar lalu masukan air. rebus sampai air habis, sesekali di aduk sambil cek tekstur oat yg dinginkan (kalo bisa menyerupai lunak nya nasi
            2. Tuang minyak, tumis bumbu yang dihaluskan sampai harum, masukan telur orak arik lalu masukan oat aduk rata
            3. Masukan garam gula kecap kaldu jamur lada, aduk rata. cek rasa, jika sudah pas rasanya nasi goreng oatmeal siap dihidangkan'];
        } else {
            $programLatihan[] = ['kategori' => 'Umur > 30', 'nama_latihan' => 'Latihan Kekuatan', 'waktu_latihan' => '40 menit', 'kalori' => '250', 'deskripsi' => '1. Posisikan tubuh berdiri tegak, lalu buka kedua kaki Anda selebar pinggul.
            2. Kemudian turunkan tubuh sejauh yang Anda bisa dengan mendorong punggung ke belakang. Lakukan gerakan ini sambil meluruskan lengan Anda ke depan untuk menjaga keseimbangan.
            3. Saat berada dalam posisi setengah jongkok, pastikan tubuh bagian bawah Anda sejajar dengan lantai dan dada harus dibusungkan tapi tidak membungkuk. 
            4. Angkat tubuh kembali ke posisi awal dan ulangi gerakan 8-12 kali dalam 1-3 set.', 'selesai' => false, 'nama_makanan' => 'Fried Shrimp Crispy Oat (Udang Goreng Krispy Gandum)', 'protein' => '10,58', 'karbohidrat' => '27,57', 'lemak' => '14,64', 'kaloris' => '287', 'resep' => '1. 12 ekor udang uk.besar/bebas
            2. Adonan basah: 2 sdm terigu + 4-5 sdm air+ 1/4sdt garam
            3. Adonan kering: 6sdm terigu
            4. 3 sdm oat
            5. 1 sdt kaldu ayam bubuk
            6. 1 sdm susu bubuk
            7. 1/4 sdt garam
            8. 1 btg cabe keriting hijau
            9. 1 btg cabe keriting merah
            10. 2 siung bawang putih
            11. 1/2 sdt lada bubuk
            12. 1 1/2 sdm butter', 'cara_masak' => '1. Siapkan 2 wadah, yg 1 untuk adonan basah, 1 lagi adonan kering. Celupkan udang ke adonan basah lalu kering lalu basah lalu kering baru goreng sampai kekuningan dan crispy dengan minyak panas. Lakukan sampai udang habis.
            2. Lalu siapkan wadah, isi dengan oat, kaldu, susu, garam, lada. Aduk rata.
            3. Kemudian panaskan wajan lagi, cairkan butternya baru masukan bawang putih yang sudah dicincang. Oseng sampai harum dan kekuningan. Kemudian masukan cabe nya disusul campuran oatnya. Aduk aduk sebentar hingga rata.
            4. Masukan udang yang sudah digoreng tadi. Aduk cepat hingga semuanya merata.
            5. Angkat, sajikan.'];
        }


        $perbedaanBeratBadan = $validatedData['berat_badan_user'] - $validatedData['berat_badan_target'];
        if ($perbedaanBeratBadan <= 5) {
            $programLatihan[] = ['kategori' => 'Berat Badan <= 5 kg', 'nama_latihan' => 'Pemanasan dan Stretching', 'waktu_latihan' => '15 menit', 'kalori' => '100', 'deskripsi' => '1. Berdiri tegak. Jaga agar kaki kanan tetap rata di lantai, tekuk lutut kanan sedikit dan rentangkan kaki kiri ke depan.
            2. Lenturkan kaki kiri, dengan tumit di lantai dan jari-jari kaki menghadap ke atas.
            3. Letakkan tangan di paha kanan dan condongkan tubuh sedikit ke depan, angkat jari-jari di kaki kiri.
            4. Tahan selama 20 detik, lalu istirahat selama 10 detik. Ulangi gerakan dengan kaki lainnya.
            5. Ulangi seluruh urutan 3 kali.', 'selesai' => false, 'nama_makanan' => 'Roti atau sereal gandum utuh', 'protein' => '5', 'karbohidrat' => '25', 'lemak' => '0', 'kaloris' => '135', 'resep' => '1. 5 sdm Tepung terigu
            2. 1 sdm Tepung maizena
            3. secukupnya Blue band
            4. 1 sdm Gula pasir
            5. 1 sachet Energen coklat', 'cara_masak' => '1. Masukkan tepung terigu tepung maizena dan blue band secukupnya sampai bisa dibuat adonan
            2. Lalu masukkan energen dan gula pasir dan meses uleni sampai bisa dibuat adonan
            3. Bentuk bulat bulat pipihkan dan tata di teflon.
            4. Panggang dengan api sangat kecil. Tutup jika bagian bawah sudah mulai agak keras balik sebentar. Jika sudah mulai keras merata angkat.'];
        } elseif ($perbedaanBeratBadan <= 10) {
            $programLatihan[] = ['kategori' => 'Berat Badan <= 10 kg', 'nama_latihan' => 'Cardio dan Kekuatan', 'waktu_latihan' => '30 menit', 'kalori' => '250', 'deskripsi' => '1. Mulai pemanasan dengan jalan di tempat, kemudian ayunkan tangan sambil berjalan ke depan dan ke belakang secara bergantian selama 3 menit.
            2. Kepalkan tangan dan rentangkan ke depan, lalu langkahkan kaki kanan ke depan dengan bagian tumit sebagai tumpuan selama 2 detik. Lakukan gerakan ini secara bergantian dengan kaki kiri. Ulangi gerakan 30 kali.
            3. Dalam posisi berdiri tegak, angkat lutut kanan hingga membentuk sudut 90° dan sentuh lutut dengan telapak tangan kiri. Lakukan hal yang sama secara bergantian dengan lutut kiri dan ulangi sebanyak 20 kali selama 30 detik.
            4. Sambil jalan di tempat, putar bahu ke arah depan dan belakang, masing-masing 5 kali.
            5. Terakhir, berdiri tegak dengan kaki sedikit terbuka dan tangan lurus ke depan. Kemudian tekuk lutut lalu berdiri. Ulangi gerakan ini sebanyak 10 kali.', 'selesai' => false, 'nama_makanan' => 'Nasi goreng oatmeal (gandum utuh)', 'protein' => '7,7', 'karbohidrat' => '53,09', 'lemak' => '12,31', 'kaloris' => '336', 'resep' => '1. 5 sdm gandum utuh
            2. 100 ml air ±
            3. 1 sdt kecap
            4. 1/4 sdt kaldu jamur
            5. secukupnya garam
            6. secukupnya lada
            7. 1 butir telur
            8. terasi sedikit (optional)', 'cara_masak' => '1. Panaskan teflon, masukan gandum utuh sangrai sebentar lalu masukan air. rebus sampai air habis, sesekali di aduk sambil cek tekstur oat yg dinginkan (kalo bisa menyerupai lunak nya nasi
            2. Tuang minyak, tumis bumbu yang dihaluskan sampai harum, masukan telur orak arik lalu masukan oat aduk rata
            3. Masukan garam gula kecap kaldu jamur lada, aduk rata. cek rasa, jika sudah pas rasanya nasi goreng oatmeal siap dihidangkan'];
        } else {
            $programLatihan[] = ['kategori' => 'Berat Badan > 10 kg', 'nama_latihan' => 'Cardio, Kekuatan, dan Diet Ketat', 'waktu_latihan' => '45 menit', 'kalori' => '400', 'deskripsi' => '1. Posisikan tubuh berdiri tegak, lalu buka kedua kaki Anda selebar pinggul.
            2. Kemudian turunkan tubuh sejauh yang Anda bisa dengan mendorong punggung ke belakang. Lakukan gerakan ini sambil meluruskan lengan Anda ke depan untuk menjaga keseimbangan.
            3. Saat berada dalam posisi setengah jongkok, pastikan tubuh bagian bawah Anda sejajar dengan lantai dan dada harus dibusungkan tapi tidak membungkuk. 
            4. Angkat tubuh kembali ke posisi awal dan ulangi gerakan 8-12 kali dalam 1-3 set.', 'selesai' => false, 'nama_makanan' => 'Fried Shrimp Crispy Oat (Udang Goreng Krispy Gandum)', 'protein' => '10,58', 'karbohidrat' => '27,57', 'lemak' => '14,64', 'kaloris' => '287', 'resep' => '1. 12 ekor udang uk.besar/bebas
            2. Adonan basah: 2 sdm terigu + 4-5 sdm air+ 1/4sdt garam
            3. Adonan kering: 6sdm terigu
            4. 3 sdm oat
            5. 1 sdt kaldu ayam bubuk
            6. 1 sdm susu bubuk
            7. 1/4 sdt garam
            8. 1 btg cabe keriting hijau
            9. 1 btg cabe keriting merah
            10. 2 siung bawang putih
            11. 1/2 sdt lada bubuk
            12. 1 1/2 sdm butter', 'cara_masak' => '1. Siapkan 2 wadah, yg 1 untuk adonan basah, 1 lagi adonan kering. Celupkan udang ke adonan basah lalu kering lalu basah lalu kering baru goreng sampai kekuningan dan crispy dengan minyak panas. Lakukan sampai udang habis.
            2. Lalu siapkan wadah, isi dengan oat, kaldu, susu, garam, lada. Aduk rata.
            3. Kemudian panaskan wajan lagi, cairkan butternya baru masukan bawang putih yang sudah dicincang. Oseng sampai harum dan kekuningan. Kemudian masukan cabe nya disusul campuran oatnya. Aduk aduk sebentar hingga rata.
            4. Masukan udang yang sudah digoreng tadi. Aduk cepat hingga semuanya merata.
            5. Angkat, sajikan.'];
        }
    
        foreach($programLatihan as $latihan){
            Program::create([
            'id_user' => $validatedData['id_user'],
            'id_form'=>$form->id,
            'kategori'=>$latihan['kategori'],
            'nama_latihan'=>$latihan['nama_latihan'],
            'waktu_latihan'=>$latihan['waktu_latihan'],
            'kalori'=>$latihan['kalori'],
            'deskripsi' => str_replace("", PHP_EOL, $latihan['deskripsi']),
            'selesai'=>$latihan['selesai'],
        ]);  
        }

        foreach($programLatihan as $latihan){
            makanan::create([
            'id_user' => $validatedData['id_user'],
            'id_form'=>$form->id,
            'nama_makanan'=>$latihan['nama_makanan'],
            'protein'=>$latihan['protein'],
            'karbohidrat'=>$latihan['karbohidrat'],
            'lemak'=>$latihan['lemak'],
            'kaloris'=>$latihan['kaloris'],
            'resep' => str_replace("", PHP_EOL, $latihan['resep']),
            'cara_masak' => str_replace("", PHP_EOL, $latihan['cara_masak']),
            'selesai'=>$latihan['selesai'],
        ]);  
        }

        return view('dashboard.index');


      

       

    
    }


    public function update(Request $request)
    {
        $user = auth()->user();

        // Validate the form if necessary
        $request->validate([
            'berat_badan_user' => 'required|numeric|min:0',
        ]);

        // Get the latest form entry for the user
        $latestForm = $user->form->latest('id')->first();

        // If there is a previous entry, update it; otherwise, create a new one
        if ($latestForm) {
            $latestForm->update([
                'berat_badan_user' => $request->berat_badan_user,
            ]);
        } else {
            $user->form()->create([
                'berat_badan_user' => $request->berat_badan_user,
            ]);
        }

        return redirect()->back()->with('success', 'Berat badan berhasil diperbarui');
    }

    
}
