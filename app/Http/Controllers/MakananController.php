<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoremakananRequest;
use App\Http\Requests\UpdatemakananRequest;
use App\Models\makanan;

class MakananController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.makanan.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function detail($id)
    {
        $user = makanan::find($id);
        return view('dashboard.makanan.detail', ['user' => $user]);
    }

    public function selesai($id)
    {
        $user = makanan::find($id);

        $user->update(['selesai' => true]);

         return redirect('/dashboard')->with('success', 'Latihan selesai.');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoremakananRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(makanan $makanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(makanan $makanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatemakananRequest $request, makanan $makanan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(makanan $makanan)
    {
        //
    }
}
