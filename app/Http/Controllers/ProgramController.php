<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProgramRequest;
use App\Http\Requests\UpdateProgramRequest;
use App\Models\Program;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function detail($id)
    {
        // Ambil data latihan berdasarkan $id dari database
        $user = program::find($id);

        // Kirim data latihan ke halaman detail
        return view('dashboard.detaillatihan.index', ['user' => $user]);
    }

    public function selesai($id)
    {
        // Temukan latihan berdasarkan ID
        $user = program::find($id);

        // Lakukan tindakan atau pemrosesan selesai di sini (misalnya, menandai sebagai selesai)
        $user->update(['selesai' => true]);

        // Redirect kembali ke halaman dashboard atau halaman lain yang sesuai
        return redirect('/dashboard')->with('success', 'Latihan selesai.');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProgramRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProgramRequest $request, Program $program)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Program $program)
    {
        //
    }
}
