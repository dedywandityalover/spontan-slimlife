<?php

namespace App\Http\Controllers;

use App\Events\RealTimeEvent;

use Illuminate\Http\Request;

use App\Models\jantung;

class RealTimeController extends Controller
{
    public function loadData()
    {

        event(new RealTimeEvent(['message' => 'Hello, real-time world!']));
        
        // $latestDetakJantung = jantung::latest()->first();

        // // Hitung status berdasarkan detak jantung
        // $detakJantung = $latestDetakJantung->detak_jantung;
        // $status = $this->hitungStatus($detakJantung);

        // $data = [
        //     'detak_jantung' => $detakJantung,
        //     'status' => $status,
        // ];

        // broadcast(new RealTimeEvent($data))->toOthers();
        // return response()->json($data);

      
    }

    // private function hitungStatus($detakJantung)
    // {
    //     // Hitung status berdasarkan range detak jantung
    //     if ($detakJantung <= 50) {
    //         return 'Sangat Rendah';
    //     } elseif ($detakJantung > 50 && $detakJantung <= 100) {
    //         return 'Normal';
    //     } elseif ($detakJantung > 100 && $detakJantung <= 200) {
    //         return 'Serangan Jantung';
    //     } else {
    //         return 'Nilai Tidak Valid';
    //     }
    // }
}
