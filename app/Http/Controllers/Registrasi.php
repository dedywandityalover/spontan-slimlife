<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
// use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Helpers\Helper;
use Twilio\Rest\Client;

class Registrasi extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('registrasi.index');
    }


    public function store(Request $request)
    {
      

        // // $id = IdGenerator::generate(['table' => 'users', 'field' => 'kode_pendaftaran', 'length' => 5, 'prefix' => 'KD']);
        // $id = Helper::IdGenerator(new User, 'kode_pendaftaran', 5, 'KD');
       

        $validatedData = $request->validate([
           
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5',
          
        ]);

        $twilioAccountSid = config('AC4582c0be33ebdea1a6f898a252b1f168');
        $twilioAuthToken = config('4b884edff1ba25a908cfc1b8616729ff');
        $pesan = "*REGISTRASI USER BARU*\nNama: {$request->name} \Email: {$request->email}";

        $twilio = new Client($twilioAccountSid, $twilioAuthToken);

        $message = $twilio->messages->create(
            "whatsapp:+6281337771929", // Ganti dengan nomor penerima WhatsApp
            [   
                "from" => "whatsapp:+14155238886",
                "body" => $pesan
            ]
        );

    
        // Simpan data ke database
        User::create($validatedData);
        
        return redirect()->route('login')->with('Sukses', 'Data berhasil disimpan');
    
    }

   
 
}