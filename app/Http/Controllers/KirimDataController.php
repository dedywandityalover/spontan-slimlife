<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KirimDataController extends Controller
{
    public function kirimdata(Request $request)
    {
        // Lakukan sesuatu dengan data yang diterima
        $data = $request->input('data');
        // ...

        // Balas dengan response JSON atau yang sesuai
        return response()->json(['message' => 'Data diterima dengan sukses']);
    }
}
