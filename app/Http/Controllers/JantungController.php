<?php

namespace App\Http\Controllers;

use App\Models\jantung;
use App\Http\Requests\StorejantungRequest;
use App\Http\Requests\UpdatejantungRequest;

class JantungController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.nadi.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function cekdetakjantung()
    {
        $jantung = jantung::select('*')->get();

        return view('dashboard.nadi.cekdetakjantung',['nilaijantung' => $jantung]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function cekstatus()
    {
        $detakJantung = jantung::select('detak_jantung')->get();

    // Determine statusMessage based on $detakJantung
    if ($detakJantung == 0) {
        $statusMessage = "Masukkan Sidik Jari";
    } elseif ($detakJantung == 100) {
        $statusMessage = "Normal";
    } elseif ($detakJantung == 200) {
        $statusMessage = "Tinggi";
    } else {
        $statusMessage = "Detak jantung tidak diketahui.";
    }

    // Return as JSON response
    return response()->json(['statusMessage' => $statusMessage]);
    }
    

    /**
     * Display the specified resource.
     */
    public function show(jantung $jantung)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(jantung $jantung)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatejantungRequest $request, jantung $jantung)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(jantung $jantung)
    {
        //
    }
}
