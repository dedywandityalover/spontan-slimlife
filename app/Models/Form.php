<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use illuminate\Database\Eloquent\Relations\BelongsTo;

class Form extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function program()
    {
        return $this->hasOne(Program::class, 'id_form');
    }


    protected $fillable = [
        'id_user',
        'jenis_kelamin',
        'umur',
        'tinggi_badan',
        'berat_badan_user',
        'berat_badan_target',
        'ukuran_badan_user',
        'ukuran_badan_target',
        'waktu_olahraga'
    ];




    protected $guarded = ['id', 'id_user'];

}
