<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use illuminate\Database\Eloquent\Relations\BelongsTo;



class makanan extends Model
{
    use HasFactory;
    public function user()
{
    return $this->belongsTo(User::class, 'id_user');
}

public function form()
{
    return $this->belongsTo(Form::class, 'id_form');
}

    protected $fillable = [
        'id_user',
        'id_form',
        'kategori',
        'nama_makanan',
        'protein',
        'karbohidrat',
        'lemak',
        'kaloris',
        'resep',
        'cara_masak',
        'selesai',
        
    ];
}
