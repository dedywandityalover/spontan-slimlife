<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Registrasi;
use App\Http\Controllers\Login;
use App\Http\Controllers\FormController;
use App\Http\Controllers\JantungController;
use App\Http\Controllers\KirimDataController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MakananController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/spontan-slimlife/kirimdata', 'KirimDataController@kirimdata');

Route::get('/', function () {
    return view('index');
});

Route::get('/login', [Login::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [Login::class, 'authenticate']);
Route::get('/logout', [Login::class, 'logout']);


Route::get('/registrasi', [Registrasi::class, 'index'])->middleware('guest');
Route::post('/registrasi', [Registrasi::class, 'store']);

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index')->middleware('auth');


Route::get('/form', [FormController::class, 'index'])->middleware('auth');
Route::post('/form', [FormController::class, 'tambah'])->middleware('auth');
Route::post('/update/berat', [FormController::class, 'update'])->name('update.berat')->middleware('auth');

Route::get('/makanan', [MakananController::class, 'index'])->middleware('auth');





Route::get('/detail-latihan/{id}', [ProgramController::class, 'detail'])->name('detail.latihan');
Route::get('/detail-makanan/{id}', [MakananController::class, 'detail'])->name('detail.makanan');

Route::post('/selesai-latihan/{id}', [ProgramController::class, 'selesai'])->name('selesai.latihan');
Route::post('/selesai-makanan/{id}', [MakananController::class, 'selesai'])->name('selesai.makanan');

Route::get('/forum', function () {
    return view('dashboard.forum.index');
})->middleware('auth');

Route::get('/toko', function () {
    return view('dashboard.toko.index');
})->middleware('auth');

Route::get('/edukasi', function () {
    return view('dashboard.edukasi.index');
})->middleware('auth');

Route::get('/edukasi1', function () {
    return view('dashboard.edukasi.edukasi1');
})->middleware('auth');

Route::get('/edukasi2', function () {
    return view('dashboard.edukasi.edukasi2');
})->middleware('auth');

Route::get('/edukasi3', function () {
    return view('dashboard.edukasi.edukasi3');
})->middleware('auth');




route::get('/nadi', [JantungController::class, 'index'])->middleware('auth');
route::get('/cekdetakjantung', [JantungController::class, 'cekdetakjantung'])->middleware('auth');

