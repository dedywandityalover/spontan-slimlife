<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
         * Run the migrations.
         *
         * @return void
         */

    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->enum('jenis_kelamin', ['Laki-laki','Perempuan']);
            $table->string('umur');
            $table->string('tinggi_badan');
            $table->string('berat_badan_user');
            $table->string('berat_badan_target');
            $table->string('ukuran_badan_user');
            $table->string('ukuran_badan_target');
            $table->string('waktu_olahraga');
            $table->timestamps();
        });
    }

    /**
         * Reverse the migrations.
         *
         * @return void
         */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
};
